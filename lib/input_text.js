var logger = loggerFactory('input_text')

Template.InputText.helpers({

})

Template.InputText.events({
    'keydown input' : function( e, tmpl ){
        logger.log(e.which)
        if( e.which == 13 ){
            // enter pressed
            Template.instance().vars.options.onComplete( tmpl.$('input').val() );
        }else if(e.which == 27){
            // escape pressed
            logger.log('escape = ');
            Template.instance().vars.options.onExit();
        }
    }
})

Template.InputText.created = function(){
    this.vars = this.view.parentView.dataVar.curValue;
    logger.log('vars', this.vars)
}
Template.InputText.rendered = function(){
    var self = this;
    self.$( "input" ).select();
}
Template.InputText.destroyed = function(){}
