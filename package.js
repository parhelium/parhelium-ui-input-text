Package.describe({
  name: 'parhelium:ui-input-text',
  version: '0.0.1',
  summary: '',
  git: '',
  documentation: 'README.md'
});

client = ['client'];
both   = ['client', 'server'];

Package.onUse(function(api) {
  api.versionsFrom('1.0.3.1');

  api.use([
      'templating',
      'parhelium:logger'
  ], client);

  api.addFiles( [
      'lib/input_text.html',
      'lib/input_text.js'
  ], client);

});
